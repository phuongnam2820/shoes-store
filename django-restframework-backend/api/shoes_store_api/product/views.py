from django.shortcuts import render
from .serializers import BrandSerializer,SizeSerializer, ProductSerializer, ProductVariantSerializer, ProductDetailSerializer, ProductImageSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from .models import Brand, Size, Product, ProductVariant, ProductImage

class BrandList(viewsets.ModelViewSet):
  queryset = Brand.objects.all()
  serializer_class = BrandSerializer

  def update(self):



class SizeList(viewsets.ModelViewSet):
  queryset = Size.objects.all()
  serializer_class = SizeSerializer



class ProductVariantList(viewsets.ModelViewSet):
  queryset = ProductVariant.objects.all()
  serializer_class = ProductVariantSerializer



class ProductList(viewsets.ModelViewSet):
  queryset = Product.objects.all()
  serializer_class = ProductSerializer

  def create(self, request, *args, **kwargs):
    productSerializer = ProductSerializer(data=request.data)
    if(productSerializer.is_valid()):
      files = request.FILES.getlist('productimages')
      productSerializer.save()
      for file in files:
              imageSerializer= ProductImageSerializer(data={'image':file,"product":productSerializer.data.get('id')})
              if imageSerializer.is_valid():
                imageSerializer.save()
    return super().create(request,*args,**kwargs)
  def update(self, request, *args, **kwargs):
    files = request.FILES.getlist('productimages')
    product = Product.objects.filter(pk=kwargs['pk']).first()
    productSerializer = ProductSerializer(product,data=request.data)
    if productSerializer.is_valid():
        ProductImage.objects.filter(product=product).all().delete()
        for file in files:
            imageSerializer= ProductImageSerializer(data={'image':file,"product":product.id})
            if imageSerializer.is_valid():
              imageSerializer.save()
    return super().update(request,*args,**kwargs)
  def get_serializer_class(self):
    if self.action == 'retrieve':
      self.serializer_class = ProductDetailSerializer

    return self.serializer_class


class ProductImageList(viewsets.ModelViewSet):
  queryset = ProductImage.objects.all()
  serializer_class = ProductImageSerializer
