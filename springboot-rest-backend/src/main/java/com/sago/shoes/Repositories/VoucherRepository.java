package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VoucherRepository extends JpaRepository<Voucher,Long> {
    List<Voucher> findAll();
    Optional<Voucher> findVoucherByCode(String name);
    boolean existsByCode(String code);
}
