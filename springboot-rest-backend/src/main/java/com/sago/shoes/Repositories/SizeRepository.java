package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Size;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SizeRepository extends JpaRepository<Size,Long> {
    List<Size> findAll();
    Optional<Size> findByName(String name);

}
