package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Product;
import com.sago.shoes.Models.ProductImage;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductImageRepository extends JpaRepository<ProductImage,Long> {
    List<ProductImage> findAll();
    void deleteAllByProduct(Product product);
}
