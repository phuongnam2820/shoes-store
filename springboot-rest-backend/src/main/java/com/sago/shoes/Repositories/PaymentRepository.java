package com.sago.shoes.Repositories;

import com.sago.shoes.Models.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment,String> {
    List<Payment> findAll();
}
