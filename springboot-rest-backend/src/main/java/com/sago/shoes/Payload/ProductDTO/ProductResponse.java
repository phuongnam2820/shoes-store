package com.sago.shoes.Payload.ProductDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Brand;
import com.sago.shoes.Models.Product;
import com.sago.shoes.Models.ProductImage;
import com.sago.shoes.Models.ProductVariant;
import lombok.Data;

import java.util.List;

@Data
public class ProductResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String slug;
    @JsonProperty
    private double price;
    @JsonProperty
    private List<ProductVariantResponse> productVariants;
    @JsonProperty
    private String brand;
    @JsonProperty
    private Long inStock;
    @JsonProperty
    private Integer sold;
    @JsonProperty
    private List<String> images;

    public ProductResponse(Long id, String name, String slug, double price, List<ProductVariantResponse> productVariants, Brand brand, List<String> images,Long inStock, Integer sold) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.price = price;
        this.productVariants = productVariants;
        this.brand = brand.getName();
        this.images = images.stream().map(str->"/images/"+str).toList();
        this.inStock =inStock;
        this.sold = sold;
    }
    public ProductResponse(Product product, List<ProductVariantResponse> productVariantResponse,Integer sold){
        this(product.getId(),product.getName(),product.getSlug(),product.getProductPrice(),productVariantResponse,product.getBrand(), product.getProductImages().stream().map(ProductImage::getImage).toList(),product.getInStock(),sold);
    }
}