package com.sago.shoes.Payload.OrderDTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Order;
import com.sago.shoes.Payload.DateResponse;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderResponse extends DateResponse {
    @JsonProperty
    private Long id;

    @JsonProperty
    private String address;

    @JsonProperty
    private String note;

    private List<OrderItemResponse> items;

    @JsonProperty
    private String status;

    @JsonProperty
    private double total;

    @JsonProperty
    private String paymentMethod;

    @JsonProperty
    private boolean isFullfilled;

    @JsonProperty
    private String customer;

    @JsonProperty
    private String phoneNumber;
    @JsonProperty
    private String staff;

    public OrderResponse(Long id, double total,String address, String note,  String status, Date createdAt, Date updatedAt, String paymentmethod, Boolean isFullfilled, String customer, String phoneNumber, String staff) {
        this.id = id;
        this.address = address;
        this.total = total;
        this.note = note;
        this.status = status;
        this.paymentMethod = paymentmethod;
        this.isFullfilled = isFullfilled;
        this.customer = customer;
        this.phoneNumber = phoneNumber;
        this.staff =staff;
        super.setCreatedAt(createdAt);
        super.setUpdatedAt(updatedAt);
    }
    public OrderResponse (Order order){
        this(order.getId(), order.getTotalPrice(),order.getAddress(), order.getNote(), order.getOrderHistories().get(order.getOrderHistories().size()-1).getOrderStatus().getName(), order.getCreatedAt(), order.getUpdatedAt(), order.getPaymentMethod(), order.isFullfilled(), order.getCustomer(), order.getPhoneNumber(),order.getOrderHistories().get(order.getOrderHistories().size()-1).getStaff().getUsername());
    }
}
