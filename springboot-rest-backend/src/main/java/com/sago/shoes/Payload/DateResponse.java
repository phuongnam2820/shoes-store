package com.sago.shoes.Payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Data
public class DateResponse {
    @JsonProperty
    private Date createdAt;
    @JsonProperty
    private Date updatedAt;
}
