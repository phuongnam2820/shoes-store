package com.sago.shoes.Payload.ProductDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Brand;
import com.sago.shoes.Models.Product;
import com.sago.shoes.Models.ProductImage;
import com.sago.shoes.Models.Review;
import lombok.Data;

import java.util.List;

@Data
public class ProductDetailResponse extends ProductResponse{

    @JsonProperty
    private List<Review> reviews;
    @JsonProperty
    private String description;

    public ProductDetailResponse(Product product, List<ProductVariantResponse> pVRs,Integer sold){
        super(product,pVRs,sold);
        this.reviews = product.getReviews();
        this.description = product.getDescription();
    }

}