package com.sago.shoes.Payload.AuthDTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class StaffSignUpRequest {
    @NotBlank
    private String fullname;

    @NotBlank
    private String username;

    @NotBlank
    private String role;

    @NotBlank
    private String password;
}
