package com.sago.shoes.Payload.VoucherDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Voucher;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data

public class VoucherResponse {

    @JsonProperty
    private Long id;
    
    @JsonProperty
    private String code;

    @JsonProperty
    private Integer quantity;

    @JsonProperty
    private Integer total;

    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    @JsonProperty
    private Long discountAmount;

    @JsonProperty
    private Long maxDiscountAmount;
    @JsonProperty
    @NotNull
    private Date expiredDate;

    public VoucherResponse(Long id, String code,Integer total, Integer quantity, String name, String description, Double discountAmount, Double maxDiscountAmount, Date expiredDate) {
        this.id = id;
        this.code = code;
        this.total = total;
        this.quantity = quantity;
        this.name = name;
        this.description = description;
        this.discountAmount = discountAmount.longValue();
        this.maxDiscountAmount = maxDiscountAmount.longValue();
        this.expiredDate = expiredDate;
    }

    public VoucherResponse(Voucher voucher){
        this(voucher.getId(),voucher.getCode(),voucher.getTotal(),
                voucher.getQuantity(),
        voucher.getName(),
        voucher.getDescription(),
        voucher.getDiscountAmount(),voucher.getMaxDiscountAmount(),voucher.getExpiredDate());

    }

}
