package com.sago.shoes.Payload.StaffDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Review;
import com.sago.shoes.Models.Staff;
import com.sago.shoes.Payload.DateResponse;
import lombok.Data;

import javax.persistence.Column;
import java.util.Date;

@Data
public class ReviewResponse extends DateResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String title;
    @JsonProperty
    private String content;
    @JsonProperty
    private Long rating;
    @JsonProperty
    private String product;


    public ReviewResponse(Long id, String name, String title, String content, Long rating, Date createdAt, Date updatedAt, String productName) {
        this.id = id;
        this.name = name;
        this.title = title;
        this.content = content;
        this.rating = rating;
        this.product = productName;
        super.setCreatedAt(createdAt);
        super.setUpdatedAt(updatedAt);
    }

    public ReviewResponse(Review review){
        this(review.getId(),review.getName(),review.getTitle(), review.getContent(), review.getRating(), review.getCreatedAt(),review.getUpdatedAt(),review.getProduct().getName());
    }
}
