package com.sago.shoes.Payload.ProductDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReviewRequest {
    @JsonProperty
    private String name;
    @JsonProperty
    private String title;
    @JsonProperty
    private Long rate;
    @JsonProperty
    private String content;
}
