package com.sago.shoes.Payload.SupplyDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SupplyOrderItemRequest {
    @JsonProperty
    private String productName;

    @JsonProperty
    private int quantity;

    @JsonProperty
    private double price;
}
