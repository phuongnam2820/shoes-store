package com.sago.shoes.Payload.OrderDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Null;
import java.util.List;

@Data
public class PaymentRequest {
    @JsonProperty
    private Long id;

    @JsonProperty
    private String description;

    @JsonProperty
    private Integer amount;

    @JsonProperty
    private String bankCode;
    @JsonProperty
    @JsonIgnore
    @Nullable
    private String ipAddress;

    public PaymentRequest(Long id, String description,Integer amount, String bankCode, String ipAddress){
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.bankCode = bankCode;
        this.ipAddress = ipAddress;
    }

}
