package com.sago.shoes.Payload.ProductDTO;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.ProductVariant;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ProductVariantResponse {
    @JsonProperty
    private String size;
    @JsonProperty
    private Map<String,String> colors;

    public ProductVariantResponse(String size, Map<String,String>  colors){
        this.size = size;
        this.colors = colors;
    }
}
