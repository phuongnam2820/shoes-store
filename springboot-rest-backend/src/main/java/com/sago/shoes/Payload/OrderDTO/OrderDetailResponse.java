package com.sago.shoes.Payload.OrderDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Order;
import com.sago.shoes.Payload.DateResponse;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class OrderDetailResponse extends DateResponse {
    @JsonProperty
    private Long id;

    @JsonProperty
    private String address;

    @JsonProperty
    private String note;

    private List<OrderItemResponse> items;

    @JsonProperty
    private List<OrderHistoryResponse> histories;

    @JsonProperty
    private Long total;


    public OrderDetailResponse(Long id, Long total, String address, String note, Date createdAt, Date updatedAt) {
        this.id = id;
        this.address = address;
        this.total = total;
        this.note = note;
        super.setCreatedAt(createdAt);
        super.setUpdatedAt(updatedAt);
    }
    public OrderDetailResponse(Order order){
        this(order.getId(), Double.valueOf(order.getTotalPrice()).longValue(),order.getAddress(), order.getNote(), order.getCreatedAt(), order.getUpdatedAt());
    }
}
