package com.sago.shoes.Payload.OrderDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sago.shoes.Models.Order;
import com.sago.shoes.Payload.DateResponse;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class OrderTrackingResponse extends OrderResponse {

    @JsonProperty
    private OrderDetailResponse orderDetailResponse;


    public OrderTrackingResponse(Long id, double total, String address, String note, String status, Date createdAt, Date updatedAt, String paymentmethod, Boolean isFullfilled, String customer, String phoneNumber, String staff,OrderDetailResponse orderDetailResponse) {
        super(id,total,address,note,status,createdAt,updatedAt,paymentmethod, isFullfilled, customer, phoneNumber, staff);
        this.orderDetailResponse = orderDetailResponse;
    }
    public OrderTrackingResponse(Order order,OrderDetailResponse orderDetailResponse){
        this(order.getId(), order.getTotalPrice(),order.getAddress(), order.getNote(), order.getOrderHistories().get(order.getOrderHistories().size()-1).getOrderStatus().getName(), order.getCreatedAt(), order.getUpdatedAt(), order.getPaymentMethod(), order.isFullfilled(), order.getCustomer(), order.getPhoneNumber(),order.getOrderHistories().get(order.getOrderHistories().size()-1).getStaff().getUsername(),orderDetailResponse);
    }
}
