package com.sago.shoes.Services;

import com.sago.shoes.Models.Staff;
import com.sago.shoes.Payload.AuthDTO.StaffSignUpRequest;
import com.sago.shoes.Payload.StaffDTO.StaffResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface StaffService {
    Staff save(Staff Staff);
    StaffResponse createStaff(StaffSignUpRequest staffSignUpRequest);
    StaffResponse updateStaff(Long id, StaffSignUpRequest staffSignUpRequest);
    Staff getStaff(String token);

    boolean existsByUsername(String username);
    Staff findByUsername(String username);
    Staff findById(Long id);

    StaffResponse getById(Long id);
    List<StaffResponse> getAllStaff();
}
