package com.sago.shoes.Services.Impl;

import com.sago.shoes.Configs.PaymentConfig;
import com.sago.shoes.Exceptions.ForbiddenException;
import com.sago.shoes.Exceptions.OutOfStockException;
import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Exceptions.InvalidVoucherException;
import com.sago.shoes.Models.*;
import com.sago.shoes.Payload.OrderDTO.*;
import com.sago.shoes.Repositories.*;
import com.sago.shoes.Services.OrderService;
import com.sago.shoes.Services.OrderStatusService;
import com.sago.shoes.Services.ProductService;
import com.sago.shoes.Services.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderHistoryRepository orderHistoryRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private VoucherService voucherService;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Object createOrder(OrderRequest orderRequest, Long userId) {

        Order order = this.convertToOrder(orderRequest);
        OrderHistory orderHistory = this.getDefaultOrderHistory(); //Pending order status
        List<OrderHistory> orderHistories = new ArrayList<>();
        orderHistories.add(orderHistory);
        User user = userRepository.getById(userId);
        order.setUser(user);
        order.setFullfilled(false);
        order.setPaymentMethod(orderRequest.getPaymentMethod());
        order.setOrderHistories(orderHistories);
        orderHistory.setOrder(order);
        orderItemRepository.saveAll(order.getOrderItems());
        orderHistoryRepository.save(orderHistory);
        Voucher voucher;
        if(orderRequest.getCode()!= null){
            voucher = voucherService.findByCode(orderRequest.getCode());
            if(voucherService.isValid(voucher,userId)){
                Double discount = order.getTotalPrice()*voucher.getDiscountAmount()/100;
                if(discount > voucher.getMaxDiscountAmount())
                    discount = voucher.getMaxDiscountAmount();
                voucherService.applyVoucher(voucher,order,user);
                order.setTotalPrice(order.getTotalPrice()-discount);
            }
            else{
                throw new InvalidVoucherException("voucher ");
            }
        }
        order = this.save(order);
        if (!order.getPaymentMethod().equalsIgnoreCase("COD")) {
            return this.createPayment(new PaymentRequest(order.getId(), "[#"+ order.getId()+ "]Thanh toan don hang SAGO SHOES", (int) order.getTotalPrice(), "NCB", orderRequest.getIpAddress()));
        }
        return this.convertToOrderResponse(order);
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public OrderResponse updateOrder(Order order, String orderStatus, String reason, String username) {
        OrderHistory orderHistory = this.createOrderHistory(order, reason, this.getDefaultOrderStatus());
        orderHistory.setOrder(order);
        //For user
        if (username.contains("@")) {
            reason = reason + " by " + username;
            orderHistory.setStaff(staffRepository.getById(1L));
        } else
            orderHistory.setStaff(staffRepository.findByUsername(username));
        orderHistory.setReason(reason);
        orderHistory.setOrderStatus(orderStatusService.getByName(orderStatus));
        orderHistoryRepository.save(orderHistory);
        if(orderStatus.equalsIgnoreCase("completed")) order.setFullfilled(true);
        order.getOrderHistories().add(orderHistory);
        return this.convertToOrderResponse(this.save(order));
    }

    @Override
    public List<OrderResponse> getAllOrders(Map<String, String> filterParam, Pageable pageable) {
        Specification<Order> filteredOrder = this.filtering(filterParam);
        List<Order> filterOrder=null;
        if(pageable==null) {
                    filterOrder = orderRepository.findAll(filteredOrder);
                    System.out.println("NOT NULL");
                }
                else
                    filterOrder = orderRepository.findAll(filteredOrder,pageable).getContent();
        if(filterParam.get("statuses") != null && filterParam.get("statuses") != "") {
            List<String> statuses = List.of(filterParam.get("statuses").split(","));
            return filterOrder.stream().filter(order ->
                    statuses.contains(this.getLastOrderStatus(order).getName().toLowerCase())
            ).map(this::convertToOrderResponse).toList();
        }
        return filterOrder.stream().map(this::convertToOrderResponse).toList();
    }
    public List<OrderResponse> getAllOrders() {
        return orderRepository.findAll().stream().map(this::convertToOrderResponse).toList();
    }

    @Override
    public List<OrderResponse> getAllOrdersOfUser(String username,Map<String, String> filterParam, Pageable pageable) {
        return orderRepository.findAll().stream().filter(order -> order.getUser().getEmail().equalsIgnoreCase(username)).map(this::convertToOrderResponse).toList();

    }

    @Override
    public OrderDetailResponse getById(Long id) {
        return orderRepository.findById(id).map(this::convertToOrderDetailResponse)
                .orElseThrow(() -> new ResourceNotFoundException("Order"));
    }


    @Override
    @Transactional(rollbackOn = OutOfStockException.class)
    public OrderResponse changeOrderStatus(Long id, String orderStatus, String reason, String username) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order ID"));
        if (this.isValidOrderStatus(order, orderStatus)) {
            // It is temporarily fixed value. (Deadline is coming :D)
            if (username.contains("@")) {
                if (orderStatus.equalsIgnoreCase("Cancelled") && this.getLastOrderStatus(order).getId() <= 2) {
                    return this.updateOrder(order, orderStatus, reason, username);
                } else throw new ForbiddenException();
            }
            if (orderStatus.equalsIgnoreCase("Shipping")) {
                for (OrderItem orderItem : order.getOrderItems()) {
                    if (!this.decreaseQuantity(orderItem.getProductVariant(), orderItem.getQuantity())) {
                        throw new OutOfStockException(orderItem.getProductVariant().getProduct().getName() + " " + orderItem.getProductVariant().getSize().getName());
                    }
                }
            }
            // It is temporarily fixed value. (Deadline is coming :D)
            // If new order status is cancelled and Its current order status is greather than or equal to 3 (Shipping Id)
            if (orderStatus.equalsIgnoreCase("Cancelled") && this.getLastOrderStatus(order).getId() >= 3) {
                for (OrderItem orderItem : order.getOrderItems()) {
                    this.increaseQuantity(orderItem.getProductVariant(), orderItem.getQuantity());
                }
            }
            return this.updateOrder(order, orderStatus, reason, username);
        }
        throw new ForbiddenException();
    }

    @Override
    @Transactional(rollbackOn = ForbiddenException.class)
    public OrderResponse cancelOrder(Long id, String reason, String username) {
        return this.changeOrderStatus(id, "Cancelled", reason, username);
    }

    @Override
    public String createPayment(PaymentRequest paymentRequest) {
        String vnp_Version = "2.1.0";
        String vnp_Command = "pay";
        String vnp_OrderInfo = paymentRequest.getDescription();
        String vnp_TxnRef = paymentRequest.getId().toString();
        String vnp_IpAddr = paymentRequest.getIpAddress();
        String vnp_TmnCode = PaymentConfig.vnp_TmnCode;
        BigInteger amount = BigInteger.valueOf((long) paymentRequest.getAmount() * 100);
        Map vnp_Params = new HashMap<>();
        vnp_Params.put("vnp_Version", vnp_Version);
        vnp_Params.put("vnp_Command", vnp_Command);
        vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
        vnp_Params.put("vnp_Amount", String.valueOf(amount));
        vnp_Params.put("vnp_CurrCode", "VND");
        String bank_code = paymentRequest.getBankCode();
        vnp_Params.put("vnp_BankCode", bank_code);
        vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
        vnp_Params.put("vnp_OrderInfo", vnp_OrderInfo);
        vnp_Params.put("vnp_OrderType", "billpayment");
        vnp_Params.put("vnp_Locale", "vn");
        vnp_Params.put("vnp_ReturnUrl", PaymentConfig.vnp_Returnurl);
        vnp_Params.put("vnp_IpAddr", vnp_IpAddr);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String vnp_CreateDate = formatter.format(orderRepository.getById(paymentRequest.getId()).getCreatedAt());
        vnp_Params.put("vnp_CreateDate", vnp_CreateDate);
        List fieldNames = new ArrayList(vnp_Params.keySet());
        Collections.sort(fieldNames);
        StringBuilder hashData = new StringBuilder();
        StringBuilder query = new StringBuilder();
        Iterator itr = fieldNames.iterator();
        try {
            while (itr.hasNext()) {
                String fieldName = (String) itr.next();
                String fieldValue = (String) vnp_Params.get(fieldName);
                if ((fieldValue != null) && (fieldValue.length() > 0)) {
                    hashData.append(fieldName);
                    hashData.append('=');
                    hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                    query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                    query.append('=');
                    query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                    if (itr.hasNext()) {
                        query.append('&');
                        hashData.append('&');
                    }
                }
            }
        } catch (Exception ex) {
            throw new ForbiddenException();
        }
        String queryUrl = query.toString();
        String vnp_SecureHash = PaymentConfig.hmacSHA512(PaymentConfig.vnp_HashSecret, hashData.toString());
        queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
        String paymentUrl = PaymentConfig.vnp_PayUrl + "?" + queryUrl;
        return paymentUrl;
    }

    @Override
    public String callbackPayment(Map<String, String> vnpay) {
        String reason = PaymentConfig.returnCode(vnpay.get("vnp_ResponseCode"));
        Order order = orderRepository.findById(Long.parseLong(vnpay.get("vnp_TxnRef"))).orElseThrow(() -> new ResourceNotFoundException("Order"));
        String transID = vnpay.get("vnp_TransactionNo");
        String description = vnpay.get("vnp_OrderInfo");
        String payDate = vnpay.get("vnp_OrderInfo");
        String bankCode = vnpay.get("vnp_BankCode");
        double amount = Double.parseDouble(vnpay.get("vnp_Amount")) / 100;
        boolean checkAmount = order.getTotalPrice() == amount;
        if (checkAmount) {
                Payment payment = this.createPayment(order, transID, description, reason, payDate, amount, bankCode);
                order.setPayment(payment);
                if ("00".equals(vnpay.get("vnp_ResponseCode"))) {
                    order.setFullfilled(true);
                    this.updateOrder(order, "Awaiting Shipment", reason, "system");
                } else {
                    this.updateOrder(order, "Cancelled", reason, "system");
                }
                return "{\"RspCode\":\"00\",\"Message\":\"Confirm Success\"}";
        } else {
            this.updateOrder(order, "Cancelled", reason, "system");
            return "{\"RspCode\":\"04\",\"Message\":\"Invalid Amount\"}";
        }
}

    @Override
    public Map<String, Object> statisticOrder() {
        Map<String,Object> summarizedData = new HashMap<>();
        Map<String, String> sellingBrands = new HashMap<>();
        Map<String,Double> sellingCities = new HashMap<>();
        Map<String,String> countedOrders = new HashMap<>();
        Map<Integer,Double> monthlyRevenue = new HashMap<>();
        List<Map<String, String>> bestSeller = new ArrayList<>();
        for(int i=1;i<=12;++i) monthlyRevenue.put(i,0.00);
        orderItemRepository.salesByBrand().stream().forEach(data->
            sellingBrands.put(data[0].toString(),data[1].toString()));

        List<OrderResponse> allOrder = this.getAllOrders();
        Double revenue = 0.00;
        for(OrderResponse orderResponse: allOrder){
            Integer numberOfOrders = countedOrders.get(orderResponse.getStatus()) == null ? 0 : Integer.parseInt(countedOrders.get(orderResponse.getStatus()).toString());
            countedOrders.put(orderResponse.getStatus(),numberOfOrders+1+"");
            String currentCity = orderResponse.getAddress().split(",")[2];
            if(orderResponse.getStatus().equalsIgnoreCase("Completed")) {
                Integer month = orderResponse.getCreatedAt().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getMonthValue();
                monthlyRevenue.put(month,orderResponse.getTotal()+monthlyRevenue.get(month));
                revenue += orderResponse.getTotal();
                sellingCities.put(currentCity,orderResponse.getTotal()+(sellingCities.get(currentCity) == null ? 0.00 : sellingCities.get(currentCity)));
            }
        }
        List<String> top5= orderItemRepository.top5BestSeller().stream().limit(8).map(productService::findProductVariant).map(productService::getFullProductName).toList();
        for(String name: top5){
            Map<String,String> map = new HashMap<>();
            map.put("name",name);
            bestSeller.add(map);
        }
        summarizedData.put("bestSeller",bestSeller);
        summarizedData.put("Revenue",revenue.toString());
        summarizedData.put("sellingBrands",sellingBrands);
        summarizedData.put("sellingCities",sellingCities);
        summarizedData.put("countedOrders",countedOrders);
        summarizedData.put("monthlyRevenue",monthlyRevenue);
        return summarizedData;
    }

    @Override
    public List<OrderTrackingResponse> tracking(String orderFilter) {
        Map<String,String> filtered = new HashMap<>();
        if (orderFilter.contains("@"))
            filtered.put("usernames",orderFilter);
        else
            filtered.put("phone",orderFilter);
        Specification<Order> filteredOrder = this.filtering(filtered);

        List<Order> filterOrder = orderRepository.findAll(filteredOrder);
        return filterOrder.stream().map(order->
            new OrderTrackingResponse (order,this.convertToOrderDetailResponse(order))
        ).toList();
    }


    @Override
    public Order save(Order entity) {
        return orderRepository.save(entity);
    }

    public Map<String,Integer> getSoldProducts(){
        Integer sold=0;
        List<OrderItem> orderItems = orderItemRepository.findAll();
        Map<String,Integer> data = new HashMap<>();
        for(OrderItem orderItem: orderItems){
            sold = data.get(orderItem.getProductVariant().getProduct().getId()+"")==null ? 0 : data.get(orderItem.getProductVariant().getProduct().getId()+"");
            data.put(orderItem.getProductVariant().getProduct().getId()+"",sold+orderItem.getQuantity());
        }
        return data;
    }
    public Integer getSoldProduct(Product product){
        Integer sold=0;
            List<OrderItem> orderItems = orderItemRepository.findAll();
            for(OrderItem orderitem: orderItems){
                if (orderitem.getProductVariant().getProduct().getId() == product.getId()){
                    sold+=orderitem.getQuantity();
                }
            }
        return sold;
    }
    private boolean isValidOrderStatus(Order order, String orderStatus) {
        OrderStatus newOrderStatus = orderStatusService.getByName(orderStatus);
        return this.getLastOrderStatus(order).getId() < newOrderStatus.getId();
    }

    private Specification<Order> filtering(Map<String, String> filterParam){
       return (Root<Order> root, CriteriaQuery<?> query,
                                            CriteriaBuilder criteriaBuilder) -> {
           List<Predicate> predicates = new ArrayList<>();
           if (filterParam.get("usernames") != null && filterParam.get("usernames") != "") {
               Arrays.stream(filterParam.get("usernames").split(",")).forEach(
                       string -> predicates.add(criteriaBuilder.or(criteriaBuilder.like(root.get("user").get("email"),
                               "%" + string + "%")))
               );
               //root.get("user").get("email").in(Arrays.stream(filterParam.get("usernames").split(",")))));
           }
           if (filterParam.get("phone") != null && filterParam.get("phone") != "") {
               Arrays.stream(filterParam.get("phone").split(",")).forEach(
                       string -> predicates.add(criteriaBuilder.or(criteriaBuilder.like(root.get("phoneNumber"),
                               "%" + string + "%")))
               );
               //root.get("user").get("email").in(Arrays.stream(filterParam.get("usernames").split(",")))));
           }
           try {
               if (filterParam.get("from") != null) {
                   Date fromDate = new SimpleDateFormat("dd/MM/yyyy").parse(filterParam.get("from"));
                   predicates.add(
                           criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), fromDate));
               }
               if (filterParam.get("to") != null) {
                   Date toDate = new SimpleDateFormat("dd/MM/yyyy 23:59:59").parse(filterParam.get("to"));
                   predicates.add(
                           criteriaBuilder.lessThanOrEqualTo(root.get("createdAt"), toDate));
               }

               query.orderBy(criteriaBuilder.desc(root.get("id")));
               return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
           } catch (Exception ex) {

               query.orderBy(criteriaBuilder.desc(root.get("id")));
               return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
           }
       };
    }

    private Payment createPayment(Order order, String transactionID, String description, String reason,String payDate, Double amount, String bankCode) {
            Payment payment = new Payment(order,transactionID,description,reason,payDate,amount, bankCode);
            return paymentRepository.save(payment);
    }

    private OrderHistory createOrderHistory(Order order, String reason, OrderStatus orderStatus) {
        OrderHistory orderHistory = new OrderHistory();
        orderHistory.setOrder(order);
        orderHistory.setReason(reason);
        orderHistory.setStaff(staffRepository.getById(2L));
        orderHistory.setOrderStatus(orderStatus);
        return orderHistory;
    }

    private OrderStatus getLastOrderStatus(Order order) {
        return order.getOrderHistories().get(order.getOrderHistories().size() - 1).getOrderStatus();
    }

    private OrderItem convertToOrderItem(OrderItemRequest orderItemRequest) {
        try {
            OrderItem orderItem = new OrderItem();
            orderItem.setProductVariant(productService.findProductVariant(orderItemRequest.getProductName()));
            orderItem.setPrice(orderItem.getProductVariant().getProduct().getProductPrice());
            orderItem.setQuantity(orderItemRequest.getQuantity());
            return orderItem;
        } catch (Exception ex) {
            throw new ResourceNotFoundException("Product Variant with %s".formatted(orderItemRequest.getProductName()));
        }
    }

    private Order convertToOrder(OrderRequest orderRequest) {
        Order order = new Order();
        List<OrderItem> orderItems = new ArrayList<>();
        double totalPrice = 0;
        for (OrderItemRequest orderItemRequest : orderRequest.getOrderItems()) {
            OrderItem orderItem = convertToOrderItem(orderItemRequest);
            orderItem.setOrder(order);
            totalPrice += orderItem.getPrice() * orderItem.getQuantity();
            orderItems.add(orderItem);
        }
        order.setAddress(orderRequest.getAddress());
        order.setNote(orderRequest.getNote());
        order.setOrderItems(orderItems);
        order.setTotalPrice(totalPrice);
        order.setOrderItems(orderItems);
        order.setCustomer(orderRequest.getCustomer());
        order.setPhoneNumber(orderRequest.getPhoneNumber());
        return order;
    }

    private OrderStatus getDefaultOrderStatus() {
        return orderStatusService.getByName("Pending");
    }

    private OrderHistory getDefaultOrderHistory() {
        OrderHistory orderHistory = new OrderHistory();
        OrderStatus orderStatus = this.getDefaultOrderStatus();
        orderHistory.setOrderStatus(orderStatus);
        orderHistory.setReason("Automatically generated by System");
        orderHistory.setStaff(staffRepository.getById(1L));
        return orderHistory;
    }

    private boolean decreaseQuantity(ProductVariant productVariant, Integer quantity) {
        Integer newQuantity = productVariant.getQuantity() - quantity;
        if (newQuantity >= 0) {
            productService.updateQuantity(productVariant, newQuantity);
            return true;
        }
        return false;
    }

    private void increaseQuantity(ProductVariant productVariant, Integer quantity) {
        Integer newQuantity = productVariant.getQuantity() + quantity;
        productService.updateQuantity(productVariant, newQuantity);
    }

    private OrderItemResponse convertToOrderItemResponse(OrderItem orderItem) {
        return new OrderItemResponse(orderItem);
    }

    private OrderHistoryResponse convertToOrderHistoryResponse(OrderHistory orderHistory) {
        return new OrderHistoryResponse(orderHistory);
    }

    private OrderResponse convertToOrderResponse(Order order) {
        List<OrderItemResponse> orderItemResponses = order.getOrderItems().stream().map(this::convertToOrderItemResponse).toList();
        OrderResponse orderResponse = new OrderResponse(order);
        orderResponse.setItems(orderItemResponses);
        return orderResponse;
    }

    private OrderDetailResponse convertToOrderDetailResponse(Order order) {
        List<OrderItemResponse> orderItemResponses = order.getOrderItems().stream().map(this::convertToOrderItemResponse).toList();
        List<OrderHistoryResponse> orderHistoryResponses = order.getOrderHistories().stream().map(this::convertToOrderHistoryResponse).toList();
        OrderDetailResponse orderDetailResponse = new OrderDetailResponse(order);
        orderDetailResponse.setItems(orderItemResponses);
        orderDetailResponse.setHistories(orderHistoryResponses);
        return orderDetailResponse;
    }

//    protected void setOutOfStockOrder(Order order,OrderItem orderItem){
//        System.out.println(" CAN THIS UPDATE?");
//        this.updateOrder(order,"Pending", productService.getFullProductName(orderItem.getProductVariant()) + " is out of stock.","system");
//    }
}
