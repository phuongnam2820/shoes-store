package com.sago.shoes.Services.Impl;

import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.Size;
import com.sago.shoes.Repositories.SizeRepository;
import com.sago.shoes.Services.SizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class SizeServiceImpl implements SizeService {

    @Autowired
    private SizeRepository SizeRepository;

    @Override
    public Size save(Size Size) {
        return SizeRepository.save(Size);
    }

    public Size create(Size size){
        return SizeRepository.save(size);
    }
    @Override
    public Size update(Long id, Size Size) {
        Size size = SizeRepository.getById(id);
        size.setName(Size.getName());
        return SizeRepository.save(size);
    }

    public Size getById(Long id) {
        return SizeRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Size")
        );
    }

    @Override
    public Size getByName(String name) {
        return SizeRepository.findByName(name).orElseThrow(() -> new ResourceNotFoundException("Size"));
    }


    @Override
    public List<Size> getAllSizes() {
        return SizeRepository.findAll();
    }


}
