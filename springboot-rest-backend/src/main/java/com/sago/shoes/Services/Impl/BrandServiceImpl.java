package com.sago.shoes.Services.Impl;

import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.Brand;
import com.sago.shoes.Repositories.BrandRepository;
import com.sago.shoes.Services.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandRepository brandRepository;

    @Override
    public Brand save(Brand brand) {
        if(brand.getSlug()==null){
            String slug = brand.getName().toLowerCase(Locale.ROOT).replaceAll(" ","-");
            brand.setSlug(slug);
        }
        return brandRepository.save(brand);
    }


    public Brand createBrand(Brand brand) {
        return brandRepository.save(brand);
    }
    public Brand updateBrand(Long id, Brand brand) {
        brand.setId(id);
        return brandRepository.save(brand);
    }

    public Brand getById(Long id) {
        return brandRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Brand")
        );
    }

    @Override
    public Brand getByName(String name) {
        return brandRepository.findBrandBySlug(name.toLowerCase().replaceAll(" ","-")).orElseThrow(()-> new ResourceNotFoundException("Brand"));
    }


    @Override
    public List<Brand> getAllBrands() {
        return brandRepository.findAll();
    }


}
