package com.sago.shoes.Services;


import com.sago.shoes.Models.Color;

import java.util.List;

public interface ColorService{
    Color save(Color color);
    Color update(Long id, Color color);
    List<Color> getAllColors();
    Color getById(Long id);
    Color getByName(String name);
}
