package com.sago.shoes.Services.Impl;

import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.*;
import com.sago.shoes.Payload.SupplyDTO.SupplyOrderItemRequest;
import com.sago.shoes.Payload.SupplyDTO.SupplyOrderItemResponse;
import com.sago.shoes.Payload.SupplyDTO.SupplyOrderRequest;
import com.sago.shoes.Payload.SupplyDTO.SupplyOrderResponse;
import com.sago.shoes.Repositories.SupplyOrderItemRepository;
import com.sago.shoes.Repositories.SupplyOrderRepository;
import com.sago.shoes.Services.ProductService;
import com.sago.shoes.Services.StaffService;
import com.sago.shoes.Services.SupplierService;
import com.sago.shoes.Services.SupplyOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class SupplyOrderServiceImpl implements SupplyOrderService {
    @Autowired
    private SupplyOrderRepository supplyOrderRepository;
    @Autowired
    private SupplyOrderItemRepository supplyOrderItemRepository;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private ProductService productService;
    @Autowired
    private StaffService staffService;
    @Override
    public SupplyOrder save(SupplyOrder SupplyOrder) {
        return supplyOrderRepository.save(SupplyOrder);
    }

    @Override
    public SupplyOrder createSupplyOrder(SupplyOrderRequest SupplyOrderRequest, Long userId) {
        SupplyOrder supplyOrder = this.convertToSupplyOrder(SupplyOrderRequest);
        supplyOrder.setStaff(staffService.findById(userId));
        supplyOrderItemRepository.saveAll(supplyOrder.getSupplyOrderItemList());
        supplyOrder = this.save(supplyOrder);
        return supplyOrder;
    }

    @Override
    public List<SupplyOrderResponse> getAllSupplyOrders(Map<String, String> filterParam, Pageable pageable) {
        Specification<SupplyOrder> filteredOrder = this.filtering(filterParam);
        List<SupplyOrder> filerredOrder = supplyOrderRepository.findAll(filteredOrder,pageable).getContent();
        if(filterParam.get("suppliers") != null && filterParam.get("suppliers") != "") {
            List<String> suppliers = List.of(filterParam.get("suppliers").split(","));
            return filerredOrder.stream().filter(order ->
                    suppliers.contains(order.getSupplier().getName().toLowerCase())
            ).map(this::convertToSupplyOrderResponse).toList();
        }

        return filerredOrder.stream().map(this::convertToSupplyOrderResponse).toList();
    }

    @Override
    public List<SupplyOrderResponse> getAllSupplyOrdersOfSupplier(String supplier, Map<String, String> filterParam, Pageable pageable) {
        return supplyOrderRepository.findAll(pageable).filter(supplyOrder -> supplyOrder.getSupplier().getName().equalsIgnoreCase(supplier)).stream().map(this::convertToSupplyOrderResponse).toList();
    }

    @Override
    public SupplyOrderResponse getById(Long id) {
        return this.convertToSupplyOrderResponse(supplyOrderRepository.getById(id));
    }


    private SupplyOrder convertToSupplyOrder(SupplyOrderRequest supplyOrderRequest){
    SupplyOrder supplyOrder = new SupplyOrder();
    supplyOrder.setSupplier(supplierService.getById(supplyOrderRequest.getSupplyName()));
    List<SupplyOrderItem> supplyOrderItems = new ArrayList<>();
    double totalPrice = 0;
    for (SupplyOrderItemRequest supplyOrderItemRequest : supplyOrderRequest.getSupplyOrderItems()) {
        SupplyOrderItem orderItem = convertToSupplyoOrderItem(supplyOrderItemRequest);
        orderItem.setSupplyOrder(supplyOrder);
        totalPrice += orderItem.getPrice() * orderItem.getQuantity();
        supplyOrderItems.add(orderItem);
    }
    supplyOrder.setTotal(totalPrice);
    supplyOrder.setSupplyOrderItemList(supplyOrderItems);
    return supplyOrder;
    }

    private SupplyOrderResponse convertToSupplyOrderResponse(SupplyOrder supplyOrder){
        List<SupplyOrderItemResponse>supplyOrderItemResponse = supplyOrder.getSupplyOrderItemList().stream().map(this::convertToSupplyoOrderItemResponse).toList();
        return new SupplyOrderResponse(supplyOrder, supplyOrderItemResponse);
    }
    private SupplyOrderItem convertToSupplyoOrderItem(SupplyOrderItemRequest supplyOrderItemRequest) {
        try {
            SupplyOrderItem supplyOrderItemOrderItem = new SupplyOrderItem();
            ProductVariant productVariant = productService.findProductVariant(supplyOrderItemRequest.getProductName());
            productVariant.setQuantity(productVariant.getQuantity()+ supplyOrderItemRequest.getQuantity());
            supplyOrderItemOrderItem.setProductVariant(productVariant);
            supplyOrderItemOrderItem.setPrice(supplyOrderItemRequest.getPrice());
            supplyOrderItemOrderItem.setQuantity(supplyOrderItemRequest.getQuantity());
            return supplyOrderItemOrderItem;
        } catch (Exception ex) {
            throw new ResourceNotFoundException("Product Variant with %s".formatted(supplyOrderItemRequest.getProductName()));
        }
    }
    private SupplyOrderItemResponse convertToSupplyoOrderItemResponse(SupplyOrderItem supplyOrderItem) {
            return new SupplyOrderItemResponse(supplyOrderItem);
    }

    private Specification<SupplyOrder> filtering(Map<String, String> filterParam){
        return (Root<SupplyOrder> root, CriteriaQuery<?> query,
                CriteriaBuilder criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            try {
                if (filterParam.get("from") != null) {
                    Date fromDate = new SimpleDateFormat("dd/MM/yyyy").parse(filterParam.get("from"));
                    predicates.add(
                            criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), fromDate));
                }
                if (filterParam.get("to") != null) {
                    Date toDate = new SimpleDateFormat("dd/MM/yyyy 23:59:59").parse(filterParam.get("to"));
                    predicates.add(
                            criteriaBuilder.lessThanOrEqualTo(root.get("createdAt"), toDate));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            } catch (Exception ex) {
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }
}
