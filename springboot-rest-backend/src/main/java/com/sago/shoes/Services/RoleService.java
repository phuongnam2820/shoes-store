package com.sago.shoes.Services;


import com.sago.shoes.Models.Role;

import java.util.List;

public interface RoleService {
    Role save(Role Role);
    Role update(Long id, Role Role);
    List<Role> getAllRoles();
    Role getById(Long id);
    Role getByName(String name);
}
