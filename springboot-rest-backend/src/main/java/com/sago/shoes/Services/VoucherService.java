package com.sago.shoes.Services;


import com.sago.shoes.Models.Order;
import com.sago.shoes.Models.User;
import com.sago.shoes.Models.Voucher;
import com.sago.shoes.Payload.VoucherDTO.VoucherRequest;
import com.sago.shoes.Payload.VoucherDTO.VoucherResponse;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;

public interface VoucherService {
    Voucher save(Voucher Voucher);
    Voucher createVoucher(VoucherRequest Voucher);
    Voucher updateVoucher(Long id,VoucherRequest Voucher);
    List<VoucherResponse> getAllVouchers(Map<String,String> filterParam, Pageable pageable);
    Voucher findById(Long id);
    Voucher findByCode(String code);
    VoucherResponse getById(Long id);
    VoucherResponse getByCode(String code);
    boolean isValid(Voucher voucher,Long userId);
    void applyVoucher(Voucher voucher,Order order, User user);
    Object validateVoucher(String code,Long userId);
}
