package com.sago.shoes.Services;


import com.sago.shoes.Models.SupplyOrder;
import com.sago.shoes.Models.SupplyOrderItem;
import com.sago.shoes.Payload.SupplyDTO.SupplyOrderRequest;
import com.sago.shoes.Payload.SupplyDTO.SupplyOrderResponse;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface SupplyOrderService {
    SupplyOrder save(SupplyOrder SupplyOrder);
    SupplyOrder createSupplyOrder(SupplyOrderRequest SupplyOrder, Long userId);
    List<SupplyOrderResponse> getAllSupplyOrders(Map<String,String> filterParam, Pageable pageable);
    List<SupplyOrderResponse> getAllSupplyOrdersOfSupplier(String supplier, Map<String,String> filterParam, Pageable pageable);
    SupplyOrderResponse getById(Long id);

}
