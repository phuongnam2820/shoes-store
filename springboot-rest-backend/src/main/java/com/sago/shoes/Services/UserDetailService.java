package com.sago.shoes.Services;

import com.sago.shoes.Security.UserDetail;

public interface UserDetailService{
    UserDetail loadUserById(Long ID);
}
