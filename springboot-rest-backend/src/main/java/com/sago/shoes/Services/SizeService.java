package com.sago.shoes.Services;


import com.sago.shoes.Models.Size;

import java.util.List;

public interface SizeService{
    Size save(Size Size);
    Size update(Long id, Size Size);
    List<Size> getAllSizes();
    Size getById(Long id);
    Size getByName(String name);
}
