package com.sago.shoes.Controllers;

import com.sago.shoes.Models.OrderStatus;
import com.sago.shoes.Services.OrderStatusService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/order-statuses")
@Api(value = "OrderStatus Resources")
public class OrderStatusController {
    @Autowired
    private OrderStatusService orderStatusService;
    @GetMapping
    @ApiOperation(value="get all OrderStatuses")
    public List<OrderStatus> getAllOrderStatuses(){
        return orderStatusService.getAllOrderStatuses();
    }

    @GetMapping("/{id}")
    @ApiOperation(value="get one OrderStatus")
    public OrderStatus getOrderStatusById(@PathVariable(value="id")Long id){
        return orderStatusService.getById(id);
    }

    @PostMapping
    @ApiOperation(value="create a new OrderStatus")
    public ResponseEntity createOrderStatus(@ApiParam(value=" OrderStatus Model") @Valid @RequestBody OrderStatus OrderStatus){
        return ResponseEntity.status(HttpStatus.CREATED).body(orderStatusService.save(OrderStatus));
    }

    @PutMapping("/{id}")
    @ApiOperation(value="update a OrderStatus")
    public ResponseEntity updateOrderStatus(
            @ApiParam(value = "id of a OrderStatus")
            @PathVariable(value="id")Long id, @Valid @RequestBody OrderStatus OrderStatus){

        return ResponseEntity.status(HttpStatus.OK).body(orderStatusService.updateOrderStatus(id,OrderStatus));
    }

    @PatchMapping("/{id}")
    @ApiOperation(value="partial update a OrderStatus")
    public ResponseEntity partialUpdateOrderStatus(int id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Partial updated" +id);
    }
}
