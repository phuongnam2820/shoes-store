package com.sago.shoes.Controllers;

import com.sago.shoes.Models.Color;
import com.sago.shoes.Services.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/colors")
@Api(value = "Color Resources")
public class ColorController {
    @Autowired
    private ColorService colorService;
    @GetMapping
    @ApiOperation(value="get all Colors")
    public List<Color> getAllColors(){
        return colorService.getAllColors();
    }

    @GetMapping("/{id}")
    @ApiOperation(value="get one Color")
    public Color getColorById(@PathVariable(value="id")Long id){
        return colorService.getById(id);
    }

    @PostMapping
    @ApiOperation(value="create a new Color")
    public ResponseEntity createColor(@Valid @RequestBody Color color){
        return ResponseEntity.status(HttpStatus.CREATED).body(colorService.save(color));
    }

    @PutMapping("/{id}")
    @ApiOperation(value="update a Color")
    public ResponseEntity updateColor(@PathVariable(value="id")Long id, @Valid @RequestBody Color color){

        return ResponseEntity.status(HttpStatus.OK).body(colorService.update(id,color));
    }

    @PatchMapping("/{id}")
    @ApiOperation(value="partial update a Color")
    public ResponseEntity partialUpdateColor(int id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Partial updated" +id);
    }
}
