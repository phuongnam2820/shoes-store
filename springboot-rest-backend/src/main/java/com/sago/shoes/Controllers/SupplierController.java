package com.sago.shoes.Controllers;

import com.sago.shoes.Models.Supplier;
import com.sago.shoes.Models.Supplier;
import com.sago.shoes.Services.SupplierService;
import com.sago.shoes.Services.SupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/suppliers")
@Api(value = "Supplier Resources")
public class SupplierController {
    @Autowired
    private SupplierService supllierService;
    @GetMapping
    @ApiOperation(value="get all Suppliers")
    public List<Supplier> getAllSupplier(){
        return supllierService.getAllSupplier();
    }

    @GetMapping("/{id}")
    @ApiOperation(value="get one Supplier")
    public Supplier getSupplierById(@PathVariable(value="id")Long id){
        return supllierService.getById(id);
    }

    @PostMapping
    @ApiOperation(value="create a new Supplier")
    public ResponseEntity createSupplier(@Valid @RequestBody Supplier supplier){
        return ResponseEntity.status(HttpStatus.CREATED).body(supllierService.createSupplier(supplier));
    }

    @PutMapping("/{id}")
    @ApiOperation(value="update a Supplier")
    public ResponseEntity updateSupplier(
            @ApiParam(value = "id of a Supplier")
            @PathVariable(value="id")Long id, @Valid @RequestBody Supplier supplier){

        return ResponseEntity.status(HttpStatus.OK).body(supllierService.updateSupplier(id,supplier));
    }

    @PatchMapping("/{id}")
    @ApiOperation(value="partial update a Supplier")
    public ResponseEntity partialUpdateSupplier(int id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Partial updated" +id);
    }
}
