package com.sago.shoes.Controllers;

import com.sago.shoes.Exceptions.ForbiddenException;
import com.sago.shoes.Models.Voucher;
import com.sago.shoes.Payload.VoucherDTO.*;
import com.sago.shoes.Payload.VoucherDTO.VoucherResponse;
import com.sago.shoes.Security.UserDetail;
import com.sago.shoes.Services.VoucherService;
import com.sago.shoes.Services.VoucherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/vouchers")
@Api(value = "Voucher Resources")
public class VoucherController {
    @Autowired
    private VoucherService voucherService;

    @GetMapping
    @ApiOperation(value = "get all Voucher")
    public List<VoucherResponse> getAllVouchers(
            @RequestParam(value="page", defaultValue = "1") Integer page,
            @RequestParam(value="limit", defaultValue = "10") Integer limit,
            @RequestParam(value="statuses",required = false, defaultValue = "available") String statuses,
            @RequestParam(value="from", required = false) String fromDate,
            @RequestParam(value="to", required = false) String toDate,
            @AuthenticationPrincipal UserDetails userDetail) {
        Map<String, String> filterParam = new HashMap<>();
        filterParam.put("statuses",statuses);
        filterParam.put("from",fromDate);
        filterParam.put("to",toDate);
        Pageable pageable = PageRequest.of(page-1,limit, Sort.by("expiredDate").descending());
        if (userDetail == null)
            return voucherService.getAllVouchers(filterParam,pageable);
        pageable = PageRequest.of(page-1,limit, Sort.by("id").descending());
        return voucherService.getAllVouchers(filterParam,pageable);

    }

    @GetMapping("/{id}")
    public VoucherResponse getVoucherById(@PathVariable(value = "id") String id) {
        if(id.matches("\\d+"))
            return voucherService.getById(Long.parseLong(id));
        return voucherService.getByCode(id);
    }

    @PostMapping
    public ResponseEntity createVoucher(@Valid @RequestBody VoucherRequest VoucherRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(voucherService.createVoucher(VoucherRequest));
    }

    @PutMapping("/{id}")
    public ResponseEntity updateVoucher(
            @ApiParam(value = "id of a Voucher")
            @PathVariable(value = "id") Long id, @Valid @RequestBody VoucherRequest voucherRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(voucherService.updateVoucher(id, voucherRequest));
    }
    
    @GetMapping("/validate")
    public ResponseEntity validateVoucher(
            @RequestParam(value="code") String code,
            @AuthenticationPrincipal UserDetail userDetail){
            if(userDetail == null) throw new ForbiddenException();
            return ResponseEntity.status(HttpStatus.OK).body(voucherService.validateVoucher(code,userDetail.getId()));
    }

    }


