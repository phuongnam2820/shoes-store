package com.sago.shoes.Controllers;

import com.sago.shoes.Exceptions.ResourceNotFoundException;
import com.sago.shoes.Models.User;
import com.sago.shoes.Payload.AuthDTO.UserSignInRequest;
import com.sago.shoes.Payload.AuthDTO.UserSignUpRequest;
import com.sago.shoes.Security.JWTokenProvider;
import com.sago.shoes.Services.StaffService;
import com.sago.shoes.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.UUID;

@RequestMapping("api/auth")
@RestController
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;
    @Autowired
    private StaffService staffService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JWTokenProvider jwTokenProvider;

    @PostMapping("/users/sign-up")
    public ResponseEntity signUp(@Valid @RequestBody UserSignUpRequest signUpRequest) {

        if (userService.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("{\"content\":\"Email exists\"}");
        }
        try {
            User user = new User(signUpRequest.getFullName(), signUpRequest.getPhoneNumber(), signUpRequest.getEmail(), passwordEncoder.encode(signUpRequest.getPassword()));
            userService.save(user);
            return ResponseEntity.status(HttpStatus.CREATED).body(user);

        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("error");
        }
    }
    @PostMapping("/users/sign-in")
    public ResponseEntity signIn(@Valid @RequestBody UserSignInRequest userSignInRequest){
      try{
          if(!userService.existsByEmail(userSignInRequest.getEmail()))
                return ResponseEntity.status(HttpStatus.ACCEPTED).body("email not found");
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userSignInRequest.getEmail(),
                            userSignInRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String Token = jwTokenProvider.createUserDetailToken(authentication);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("{\"token\" :\"" +Token+"\" }");
    }catch(Exception ex){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("{\"content\":\"Wrong username or password\"}");
    }
    }
    @PostMapping("/staffs/sign-in")
    public ResponseEntity staffSignIn(@Valid @RequestBody UserSignInRequest staffSignInRequest) {
        try {

            if (!staffService.existsByUsername(staffSignInRequest.getEmail()))
                return ResponseEntity.status(HttpStatus.ACCEPTED).body("email not found");
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            staffSignInRequest.getEmail(),
                            staffSignInRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String Token = jwTokenProvider.createStaffDetailToken(authentication);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("{\"token\" :\"" + Token + "\" }");
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("{\"content\":\"Wrong username or password\"}");
        }
    }

}