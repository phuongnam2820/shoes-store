package com.sago.shoes.Exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class ResourceExistsException extends RuntimeException{
    public ResourceExistsException(String resource){
        super(resource+" exists");
    }
}
