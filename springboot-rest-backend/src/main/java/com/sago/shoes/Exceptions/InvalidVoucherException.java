package com.sago.shoes.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class InvalidVoucherException extends RuntimeException{
    public InvalidVoucherException(String resource){
        super(resource);
    }
}
