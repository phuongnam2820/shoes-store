package com.sago.shoes.Exceptions;

import lombok.Data;

@Data
public class ExceptionResponse {
    private final String message;

}
