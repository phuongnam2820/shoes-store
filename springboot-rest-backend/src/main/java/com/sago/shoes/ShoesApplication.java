package com.sago.shoes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sago.shoes.Security.JWTAuthenticationFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import com.sago.shoes.Configs.FileConfig;
import com.sago.shoes.Payload.ProductDTO.ProductRequest;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.stereotype.Component;


@SpringBootApplication
@EnableConfigurationProperties(FileConfig.class)
@EnableJpaAuditing
public class ShoesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoesApplication.class, args);
    }

    @Bean
    public JWTAuthenticationFilter JWTAuthenticationFilter() {
        return new JWTAuthenticationFilter();
    }

    @Component
    public static class StringToUserConverter implements Converter<String, ProductRequest> {

        @Autowired
        private ObjectMapper objectMapper;

        @SneakyThrows
        public ProductRequest convert(String source) {
            return objectMapper.readValue(source, ProductRequest.class);
        }
    }


}
