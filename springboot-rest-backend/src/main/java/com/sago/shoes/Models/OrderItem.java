package com.sago.shoes.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Table(name="api_order_item")
@Entity
public class OrderItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name="order_id")
    @JsonIgnore
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productVariant_id")
    @JsonIgnore
    private ProductVariant productVariant;

    @Column(name = "quantity")
    @Min(value = 0, message = "Product quantity must no be less then zero.")
    private int quantity;

    @Column(name="price")
    @Min(value = 0, message = "Product price must no be less then zero.")
    private double price;
}
