package com.sago.shoes.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name="api_order")
public class Order extends DateEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name ="price")
    private double totalPrice;

    @Column
    private String address;

    @Column
    private String customer;

    @Column
    private String phoneNumber;

    @Column(nullable = false)
    private boolean isFullfilled = false;

    @Column
    private String note;

    @Column
    private String paymentMethod = "COD";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private User user;

    @OneToOne(mappedBy = "order",fetch = FetchType.LAZY)
    private Payment payment;

    @OneToMany(mappedBy = "order")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<OrderHistory> orderHistories;

    @OneToMany(mappedBy = "order")
    @Fetch(value = FetchMode.SUBSELECT)

    private List<OrderItem> orderItems;



}
