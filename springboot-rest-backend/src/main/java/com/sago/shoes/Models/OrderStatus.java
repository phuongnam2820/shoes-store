package com.sago.shoes.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name="api_order_status")
public class OrderStatus extends DateEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "orderStatus", fetch = FetchType.LAZY)
    @JsonIgnore
    @Fetch(value = FetchMode.SUBSELECT)
    private List<OrderHistory> orderHistories;


    private String description;


}
