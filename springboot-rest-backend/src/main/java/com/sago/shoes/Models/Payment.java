package com.sago.shoes.Models;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name="api_payment")
@NoArgsConstructor
public class Payment extends DateEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="id")
    private String id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    private Order order;

    @Column
    private double amount;
    @Column(name= "bank_code")
    private String bankCode;

    @Column
    private String orderInfo;

    @Column
    private String reason;
    @Column
    private String payDate;

    public Payment(Order order, String transactionID, String description, String reason,String payDate, Double amount, String bankCode){
        this.id = transactionID;
        this.order = order;
        this.amount = amount;
        this.bankCode = bankCode;
        this.orderInfo = description;
        this.reason = reason;
        this.payDate = payDate;
    }
}
