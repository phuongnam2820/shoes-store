package com.sago.shoes.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@Table(name="api_staff",uniqueConstraints ={@UniqueConstraint(columnNames = {"username"})})
public class Staff extends DateEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "fullName")

    private String fullName;

    @JsonIgnore
    @NotBlank
    @Size(max = 100)
    @Column(name = "password")
    private String password;

    @NotBlank
    @NaturalId
    @Size(max = 40)
    @Column(name = "username")
    private String username;

    @ManyToOne(targetEntity = Role.class, fetch = FetchType.LAZY)
    @JoinColumn(name="role_id",nullable = false)
    @JsonIgnore
    private Role role;

    public Staff(String fullName, String username, String password, Role role){
        this.fullName = fullName;
        this.username =username;
        this.password = password;
        this.role = role;
    }

}
