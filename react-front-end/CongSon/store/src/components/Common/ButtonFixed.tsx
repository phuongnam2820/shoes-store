import * as React from 'react';
import { Link } from 'react-router-dom';

export interface ButtonFixedProps {
}

export function ButtonFixed(props: ButtonFixedProps) {
    return (
        <>
            <Link to="/vouchers" className='fixed bottom-10 animate-bounce right-12 z-50 border rounded-full p-4 border-purple-500 bg-purple-500'>
                <img className='h-11 w-11' src="https://img.icons8.com/dotty/50/ffffff/discount-ticket.png" />
            </Link>
        </>
    );
}
