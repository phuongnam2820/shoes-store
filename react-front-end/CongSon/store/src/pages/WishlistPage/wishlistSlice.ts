import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface WishlistState {
  wishList: any;
}

const initialState: WishlistState = {
  wishList: JSON.parse(localStorage.getItem("wishlist") || "[]"),
};

export const wishlistSlice = createSlice({
  name: "wishlist",
  initialState,
  reducers: {
    addToWishList: (state, action) => {
      const newItem = action.payload;

      const index = state.wishList.findIndex(
        (item: any) => item.id === newItem.id
      );
      if (index >= 0) {
        state.wishList.splice(index, 1);
        localStorage.setItem("wishlist", JSON.stringify(state.wishList));
        return;
      }
      state.wishList.push(newItem);
      localStorage.setItem("wishlist", JSON.stringify(state.wishList));
    },
    removeFromWishlist: (state, action) => {
      console.log(action.payload);
      const idRemove = action.payload;
      state.wishList = state.wishList.filter(
        (item: any) => item.id !== idRemove
      );
      localStorage.setItem("wishlist", JSON.stringify(state.wishList));
    },
  },
});
export const { addToWishList, removeFromWishlist } = wishlistSlice.actions;
export const selectWishlist = (state: RootState) => state.wishlist.wishList;
export default wishlistSlice.reducer;
