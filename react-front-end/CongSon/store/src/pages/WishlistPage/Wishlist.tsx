import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useAppSelector } from '../../hooks';
import { WishlistItem } from './components/WishlistItem';
import { wishlistItemsCountSelector } from './selector';
import { EmptyPage } from '../../components/Common/EmptyPage';
import { removeFromWishlist } from './wishlistSlice';
export interface WishlistProps {
}

export function Wishlist(props: WishlistProps) {
    const wishlist = useAppSelector(state => state.wishlist.wishList);
    const wishlistItemsCount = useSelector(wishlistItemsCountSelector);
    const distpach = useDispatch();
    const handleProductRemove = (idRemove: any) => {
        const actions = removeFromWishlist(idRemove);
        distpach(actions);
    }

    return (
        <>
            {wishlistItemsCount > 0 ?

                <div className="mx-auto py-10 max-w-screen-xl flex">
                    <div className="flex flex-col">
                        <div className="mt-3">
                            <h1 className="text-3xl lg:text-4xl tracking-tight font-semibold leading-8 lg:leading-9 text-gray-800">Favourites</h1>
                        </div>
                        <div className="mt-4">
                            <p className="text-2xl tracking-tight leading-6 text-gray-600">0{wishlistItemsCount} items</p>
                        </div>
                        <div className="mt-10 lg:mt-12 grid grid-cols-1 lg:grid-cols-3 gap-x-8 gap-y-10 lg:gap-y-0">
                            {wishlistItemsCount > 0 ?
                                wishlist.map((product: any) => <WishlistItem product={product} handleProductRemove={handleProductRemove} />)
                                :
                                <EmptyPage title="There are no items in your wishlist" />
                            }
                        </div>
                    </div>
                </div>
                :
                <EmptyPage title="There are no items in your wishlist." />
            }
        </>
    );
}
