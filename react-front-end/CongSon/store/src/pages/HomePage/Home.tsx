import * as React from 'react';
import { Header } from '../../components/Common/Header';
import { Footer } from '../../components/Common/Footer';
import { Policy } from './Policy';
import { RelatedProduct } from './Related';
import { Hero } from './Hero';
import { Banner } from './Banner';
import { Category } from './Category';
import { Voucher } from './Voucher';
import { ButtonFixed } from '../../components/Common/ButtonFixed';


export function HomePage() {
  
  return (
    <div>
      <Header />
      <Hero />
      <Category />
      <Policy />
      <Voucher />
      <RelatedProduct />
      <ButtonFixed />
      <Banner />
      <Footer />
      
    </div>
  );
}
