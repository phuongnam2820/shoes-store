import * as React from 'react';
import axiosClient from '../../../../api/axios-client';
import { ProductCard } from '../ProductCard';
export interface RelatedProductsProps {
}

export function RelatedProducts(props: RelatedProductsProps) {

  const [ productList, setProductList ] = React.useState([]);

  const fetchData = async () => {
    try {
      let result:any = await axiosClient.get('/products?limit=4');
      result.forEach((item:any) => item.slug = `/products/${item.slug}`);
      setProductList(result);
    } catch (error) {
      
    }
  }

  React.useEffect(() => {
    fetchData();
  },[])

  return (
    <div className="container mt-10">
      <h3 className="font-bold text-my-color uppercase text-2xl">YOU MIGHT ALSO LIKE </h3>
      <div className="grid grid-cols-2 md:grid-cols-3 xl:grid-cols-4 grid-flow-row">
        {productList.map(product => <ProductCard product={product} />)}
      </div>
    </div>
  );
}
