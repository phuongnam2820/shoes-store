import * as React from 'react';

export interface SizeProps {
    sizeList: string[],
    selectedSize?: string,
    handleChange: Function
}

export function Size(props: SizeProps) {
    const { handleChange, sizeList, selectedSize } = props;
    const [isOpen, setOpen] = React.useState(false);
    const handlePopup = () => {
        setOpen(!isOpen);
    }

    const handleSizeChange = (e: any) => {
        handleChange(e.target.value);
    }
    return (
        <div className="mt-10 items-center">
            <div className="flex justify-between mb-1">
                <h3 className="font-medium">Select Size</h3>
                <button className="text-gray-400" onClick={handlePopup}>Size Guide</button>
            </div>
            {isOpen ?
                <div className="fixed text-gray-500 flex items-center justify-center overflow-auto z-50 bg-black bg-opacity-40 left-0 right-0 top-0 bottom-0">
                    <div className="bg-white rounded-xl shadow-2xl p-6 w-full mx-10 justify-center">
                        <img className="mx-auto w-6/12" src="https://tokushoes.com/wp-content/uploads/2018/07/how-to-measure-your-shoes-size.jpg" />
                        <button onClick={handlePopup} className="block mx-auto text-gray-500 cursor-pointer font-bold py-2 px-4 rounded my-3 items-end">Close</button>
                    </div >
                </div>
                :
                null
            }
            <div className="flex flex-wrap">
                {sizeList.map((item,index) => {
                    return (
                        <label className="inline-flex m-0.5 cursor-pointer" key={index}>
                            <input value={item} type="radio" className="form-radio h-5 w-full hidden" onClick={handleSizeChange} /><span className={`border-2 px-8 py-5 transition ease-in duration-75 ${selectedSize?.includes(item) ? " bg-black text-white border-current" : "hover:bg-black hover:text-white hover:border-current"}`}>{item}</span>
                        </label>
                    )
                })}
            </div>
        </div>
    );
}
