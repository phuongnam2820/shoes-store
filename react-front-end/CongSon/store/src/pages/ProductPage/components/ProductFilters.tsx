import * as React from 'react';
import { FilterByCategory } from './Filters/FilterByCategory';
import { FilterByPrice } from './Filters/FilterByPrice';
import { FilterByBrand } from './Filters/FilterByBrand';
import { FilterBySize } from './Filters/FilterBySize';
import { FilterByColor } from './Filters/FilterByColor';
import axiosClient from '../../../api/axios-client';

interface ProductFilterProps {
    filters: any,
    onChange: Function,
}


let colorListData = [

    {
        id: 1,
        name: "Red",
        slug: "red"
    },
    {
        id: 2,
        name: "Green",
        slug: "green"
    },
    {
        id: 3,
        name: "Yellow",
        slug: "yellow"
    },
    {
        id: 4,
        name: "Blue",
        slug: "blue"
    },
    {
        id: 5,
        name: "Gray",
        slug: "gray"
    },
    {
        id: 6,
        name: "Pink",
        slug: "pink"
    },
    {
        id: 7,
        name: "Cyan",
        slug: "cyan"
    },
    {
        id: 8,
        name: "Purple",
        slug: "purple"
    },
]

export function ProductFilters({ filters, onChange }: ProductFilterProps) {

    const [colorList, setColorList] = React.useState(colorListData);
    const [brandList, setBrandList] = React.useState([]);
    const [sizeList, setSizeList] = React.useState([]);

    const fetchBrand = async () => {
        try {
            let result: any = await axiosClient.get("/brands");

            if (!result) {
                return;
            }
            setBrandList(result);
        } catch (error) {
        }
    }
    const fetchSize = async () => {
        try {
            let result: any = await axiosClient.get("/sizes");
            
            if (!result) {
                return;
            }
            setSizeList(result);
        } catch (error) {
        }
    }

    React.useEffect(() => {
        fetchBrand();
        fetchSize();
    }, [])

    const handleChange = (newFilters: any) => {
        onChange(newFilters);
    }

    return (
        <>
            <div className="hidden lg:block w-60 sticky h-screen top-1 overflow-y-scroll">
                <FilterByCategory onChange={handleChange} />
                <FilterByColor colorList={colorList} onChange={handleChange} filters={filters} />
                <FilterByPrice onChange={handleChange} filters={filters} />
                {brandList.length > 0 && <FilterByBrand brandList={brandList} onChange={handleChange} filters={filters} /> }
                {sizeList.length >  0 && <FilterBySize sizeList={sizeList} onChange={handleChange} filters={filters} /> }
            </div>
        </>


    )
}