import * as React from 'react';
import { toast } from 'react-toastify';
interface IVoucher {
    id: number,
    code: string,
    quantity: number,
    name: any,
    description: string,
    expiredDate: any,
    progress: number
}

export interface IVoucherItemProps {
    voucher: IVoucher
}



export function VoucherItem({ voucher }: IVoucherItemProps) {

    const handleVoucherClick = () => {
        if(localStorage.getItem('voucher') === voucher.code){
            toast.warn('This voucher has been saved');
            return;
        }
        localStorage.setItem('voucher',voucher.code);
        toast.success("Voucher has been saved");
    }

    return (
        <div className='flex flex-col md:flex-row border border-gray-200 p-5 md:w-4/6 mx-auto my-5' key={voucher.id}>
            <div className='flex flex-1 items-center'>
                <div className='border-r-2 w-1/5 md:1/6 h-24 rounded border-red-500 items-center flex flex-col border justify-center'>
                    <span className="truncate w-full text-3xl mb-2 font-bold leading-9 text-red-500 text-center">{voucher.name.match(/(\d+%)/) ? voucher.name.match(/(\d+%)/)[1] : 'Sale'}</span>
                    <p className='bg-red-500 text-white w-full text-center'>Promo</p>
                </div>
                <div className="ml-5 flex flex-col justify-center w-full">
                    <span className='flex'>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-red-500 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                        {voucher.expiredDate}
                    </span>
                    <h2 className="text-xl font-medium leading-normal mt-0 mb-2 text-black">
                        {voucher.name}
                    </h2>
                    <p className='text-gray-400 text-sm'>{voucher.description}</p>
                    <div className='w-1/5 '>
                        <span className='text-gray-400 text-xs'>{voucher.progress}% left</span>
                        <div className="w-full bg-gray-300 h-1 mt-3">
                            <div className="bg-red-600 h-1 border-l-2 border-r-2 rounded-xl border-transparent" style={{ width: `${voucher.progress}%` }}></div>
                        </div>
                    </div>

                </div>
            </div>

            <div className='justify-center items-center flex mt-5 md:mt-0'>
                <button className="mx-2 my-2 w-full bg-white transition duration-200 ease-in-out rounded border border-indigo-700 text-indigo-700 px-6 py-2 text-xs hover:text-gray-50 hover:bg-indigo-700"
                onClick={handleVoucherClick}
                >
                    Get code
                </button>
            </div>
        </div>
    );
}
