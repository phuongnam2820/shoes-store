import * as React from 'react';
import { toast } from 'react-toastify';
import axiosClient from '../../api/axios-client';
import { VoucherItem } from './components/VoucherItem';
import moment from 'moment';
export interface VoucherProps {
}

export function Voucher(props: VoucherProps) {


    const [voucherList, setVoucherList] = React.useState([]);

    const remainingTime = (time: any) => {
        let expiredDate: any = moment(time).unix();
        let currTime: any = moment().unix();
        let leftTime = expiredDate - currTime;
        if (leftTime <= 0) {
            return null;
        }
        var duration = moment.duration(leftTime, 'seconds');
        return duration;
    }

    const fetchData = async () => {
        try {
            const result: any = await axiosClient.get("/vouchers");
            if (result) {
                let duration;
                let newVoucherList = result.filter((voucher: any) => {
                    duration = remainingTime(voucher.expiredDate);
                    if (duration) {
                        voucher.expiredDate = `${duration.days()} days ${duration.hours()}:${duration.minutes()} left`;
                        voucher.progress = voucher.quantity * 100 / voucher.total;
                        return voucher;
                    }
                    return null;
                });
                setVoucherList(newVoucherList);
            }
        } catch (error) {
            toast.error("Something went wrong");
        }
    }

    React.useEffect(() => {
        fetchData();
    },[])

    return (
        <div className='mt-5'>
            <div className="py-10 w-full flex flex-col items-center justify-center relative">
                <h1 className="text-4xl font-bold leading-9 text-center">Our discount code today!</h1>
                <p className="text-base leading-normal text-center text-black mt-6">
                    Find the latest Sago promo codes and discount vouchers here. Buy for less!
                </p>
            </div>
            {voucherList && voucherList.map((voucher: any) => <VoucherItem voucher={voucher} />)}

        </div>
    );
}
